package com.myzee.beans;

import javax.annotation.Resource;

public class Employee {
	private String id;
	private String name;
	
	@Resource(name="comp1")
	private Company company;
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "Employee - " + this.getName() + company.toString();
	}
	
}
