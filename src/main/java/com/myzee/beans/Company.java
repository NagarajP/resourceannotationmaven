package com.myzee.beans;

public class Company {
	private String name;
	private String location;
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLocation() {
		return location;
	}
	
	@Override
	public String toString() {
		return "; Company - " + this.getName() + "; Location - " + this.getLocation();
	}
}
