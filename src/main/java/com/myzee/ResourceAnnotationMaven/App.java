/*
 * @Resource wires bean by name
 * @Autowired wires bean by type
 * @Autowrired along with @Qualifier wires bean by name
 */

package com.myzee.ResourceAnnotationMaven;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.myzee.beans.Employee;

public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext ap = new ClassPathXmlApplicationContext("com/myzee/resources/spring.xml");
        Employee e = (Employee)ap.getBean("emp");
        System.out.println(e.toString());
    }
}
